import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, 'page01');
              },
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(250, 50),
                backgroundColor: Colors.indigo,
                textStyle: const TextStyle(
                  fontSize: 18,
                ),
              ),
              child: const Text('Favorite pet'),
            ),
            const SizedBox(height: 60),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, 'page02');
              },
              style: ElevatedButton.styleFrom(
                minimumSize: const Size(250, 50),
                backgroundColor: Colors.indigo,
                textStyle: const TextStyle(
                  fontSize: 18,
                ),
              ),
              child: const Text('I love animals'),
            )
          ],
        ),
      ),
    );
  }
}

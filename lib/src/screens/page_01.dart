import 'package:flutter/material.dart';

class Page01 extends StatelessWidget {
  const Page01({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Favorite pet'),
          backgroundColor: Colors.pink,
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Image(
              image: NetworkImage(
                  'https://www.fanaticosdelasmascotas.cl/wp-content/uploads/2022/07/gato_edad_natali_kuzina_shutterstock_portada.jpg'),
              width: 500,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Beautiful cat',
              style: TextStyle(
                  fontSize: 40,
                  fontFamily: 'Lucida calligraphy',
                  color: Colors.blueGrey),
            ),
          ],
        )));
  }
}

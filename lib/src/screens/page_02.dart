import 'package:flutter/material.dart';

class Page02 extends StatelessWidget {
  const Page02({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('I love animals'),
          backgroundColor: Colors.blueGrey,
        ),
        body: Center(
            child: ListView(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: [
            cardImage('assets/images/gato1.jpg', 'Beautiful cat Tomás'),
            cardImage('assets/images/perro1.jpeg', 'Beautiful dog Pedro '),
            cardImage('assets/images/gato2.jpg', 'Beautiful cat Nala'),
            cardImage(
                'assets/images/perro2.jpg', 'Beautiful Lulu and Lala and Lili'),
            cardImage('assets/images/gato3.jpg', 'Beautiful black cat'),
            cardImage('assets/images/perro3.jpg', 'Beautiful dog Max'),
            cardImage('assets/images/gato1.jpg', 'Beautiful cat Tomás'),
            cardImage('assets/images/perro1.jpeg', 'Beautiful dog Pedro '),
            cardImage('assets/images/gato2.jpg', 'Beautiful cat Nala'),
            cardImage(
                'assets/images/perro2.jpg', 'Beautiful Lulu and Lala and Lili'),
            cardImage('assets/images/gato3.jpg', 'Beautiful black cat'),
            cardImage('assets/images/perro3.jpg', 'Beautiful dog Max'),
          ],
        )));
  }

  Widget cardImage(image, texto) {
    return Card(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Image(
            image: AssetImage(image),
            width: 200,
            height: 100,
            fit: BoxFit.cover,
          ),
          const SizedBox(
            width: 20,
          ),
          Text(
            texto,
            style: const TextStyle(
                fontSize: 18,
                fontFamily: 'Lucida calligraphy',
                color: Colors.indigo),
          ),
        ],
      ),
    ));
  }
}

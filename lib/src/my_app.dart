import 'package:challenge02/src/screens/home_screen.dart';
import 'package:challenge02/src/screens/page_01.dart';
import 'package:challenge02/src/screens/page_02.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: const HomeScreen(),
        routes: {
          'page01': (context) => const Page01(),
          'page02': (context) => const Page02(),
        });
  }
}
